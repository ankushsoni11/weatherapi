package com.weather.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.weather.exception.WeatherBadRequest;
import com.weather.model.WeatherOutput;
import com.weather.service.WeatherService;

@RestController
@RequestMapping("/weather")
public class WhetherController {

	@Autowired
	private WeatherService weatherService;

	@RequestMapping(method = RequestMethod.GET, value = "/forecast/{city}")
	public  ResponseEntity<WeatherOutput> getWhetherforcast(@PathVariable("city") String city) throws Exception {
		
		if(city.length()<=1)
			throw new WeatherBadRequest("Need to enter City Name");
		WeatherOutput weatherOutput = weatherService.getWheatherForcast(city);
		
	
         
        //Send location in response
        HttpHeaders headers = new HttpHeaders();
        headers.add("Total-count", String.valueOf(weatherOutput.getDays().size()));
        return new ResponseEntity(weatherOutput, headers, HttpStatus.OK);
        
	}

}
