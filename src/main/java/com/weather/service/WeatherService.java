package com.weather.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.weather.model.Day;
import com.weather.model.WeatherData;
import com.weather.model.WeatherOutput;

@Service
public class WeatherService {

	private String wherther_fetch_url = "http://api.openweathermap.org/data/2.5/forecast?q=city&mode=json&appid=d2929e9483efc82c82c32ee7e02d563e";

	private Integer numberOfDays = 3; // we can configure in property files.

	public WeatherOutput getWheatherForcast(String city) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		WeatherData result = restTemplate.getForObject(wherther_fetch_url.replace("city", city), WeatherData.class);
		return this.getNextThreeDaysSuggestion(result);
	}

	private WeatherOutput getNextThreeDaysSuggestion(WeatherData weatherData) {
		WeatherOutput weatherOutPut = new WeatherOutput();
		
		weatherOutPut.setCityName(weatherData.getCity().getName());
		List<com.weather.model.List> wlist = weatherData.getList();

		for (int i = 0; i < numberOfDays; i++) {

			String high = String.valueOf(wlist.get(i).getMain().getTempMax());
			String low = String.valueOf(wlist.get(i).getMain().getTempMin());
			String remark = null;
			if (wlist.get(i).getMain().getTempMax() > 313.15)
				remark = "Use Sunscreen lotion";

			for (com.weather.model.Weather weather : wlist.get(i).getWeather()) {
				if (weather.getMain().contains("Rain"))
					remark = "Carry Umberella";
			}
			Day day = new Day(high, low, remark);
			weatherOutPut.addDay(day);

		}
		return weatherOutPut;

	}

}
