package com.weather.model;

import java.util.ArrayList;
import java.util.List;

public class WeatherOutput {
	
	String cityName;
	
	String date;
	
	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	List<Day> listDay;
	
	public WeatherOutput()
	{
		listDay = new ArrayList<Day>();
	}

	public void addDay(Day day) {
		listDay.add(day);
	}
	
	public List<Day> getDays()
	{
		return listDay;
	}
}
