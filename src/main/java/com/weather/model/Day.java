package com.weather.model;

public class Day {
	
	public String high;
	
	public String low;
	
	public String remark;

	public Day(String high, String low, String remark) {
		super();
		this.high = high;
		this.low = low;
		this.remark = remark;
	}

	public String getHigh() {
		return high;
	}

	public void setHigh(String high) {
		this.high = high;
	}

	public String getLow() {
		return low;
	}

	public void setLow(String low) {
		this.low = low;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
