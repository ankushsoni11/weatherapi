package com.weather;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.weather.ServiceApplication;

@RunWith(SpringRunner.class)
@SpringBootTest
class ServiceApplicationTests {

	@Test
	void contextLoads() {
		ServiceApplication.main(Arrays.array());
	}

}
