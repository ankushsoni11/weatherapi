package com.weather.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import com.weather.ServiceApplication;
import com.weather.model.Day;
import com.weather.model.List;
import com.weather.model.Main;
import com.weather.model.WeatherData;
import com.weather.model.WeatherOutput;
import com.weather.service.WeatherService;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ServiceApplication.class)
@AutoConfigureMockMvc
public class WeatherControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@InjectMocks
	WhetherController weatherController;
	
	@Mock
	WeatherService weatherService;
	
	private HttpMessageConverter mappingJackson2HttpMessageConverter;
	
	@Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();
        Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
    }

	@SuppressWarnings("unchecked")
    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
	
	@Test
	public void should_return_bad_request() throws Exception {
		mockMvc.perform(get("/weather/forecast/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());

	}  
	
	@Test
	public void testWeatherForecastCount() throws Exception {
		mockMvc.perform(get("/weather/forecast/London").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(header().string("Total-count", "3"));

	}  

	@Test
	public void testweatherforecaseJunit() throws Exception {
		 MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
		 RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(mockHttpServletRequest));
		 
		 WeatherOutput output = new WeatherOutput();
		 
		 Day day1 = new Day("400", "200", "Use Sunscreen lotion");
		 Day day2 = new Day("399", "199", "Use Sunscreen lotion");
		 Day day3 = new Day("398", "198", "Use Sunscreen lotion");
		 
		 output.addDay(day1);
		 output.addDay(day2);
		 output.addDay(day3);
		 
		 when(weatherService.getWheatherForcast(any(String.class))).thenReturn(output);
		 
		 ResponseEntity<WeatherOutput> responseEntity = weatherController.getWhetherforcast("London");
		 
		 assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
	     assertThat(responseEntity.getHeaders().get("Total-count").toString()).isEqualTo("[3]");
	     
	}

}
